# UNIDAD III: Listas enlazadas.

## OBJETIVOS PARTICULARES DE LA UNIDAD

El alumno resolverá problemas de ingeniería y ciencias, utilizando las estructuras lineales enlazadas dinámicamente, así como las operaciones que se efectúan sobre ellas.

| No. Tema  | Título Tema                                    | Teoría              | Práctica            | Evaluación Continua | Clave Bibliográfica                                                         |
| :---      | :----                                          | :---:               | :---:               | :---:               | :---:                                                                       |
| 3.1       | CONCEPTOS BASICOS                              | :white_check_mark:  | :x:                 | :white_check_mark:  | [1B,2B,3B](activos/pdfs/CONCEPTOS_BASICOS.pdf)                              |
| 3.2       | OPERADORES EN LISTAS                           | :white_check_mark:  | :x:                 | :white_check_mark:  | [1B,2B,3B](activos/pdfs/OPERADORES_EN_LISTAS.pdf)                           |
| 3.2.1     | Creación e Inserción                           | :white_check_mark:  | :white_check_mark:  | :x:                 | [1B,2B,3B](activos/pdfs/Creacion_e_Insercion.pdf)                           |
| 3.2.2     | Borrado                                        | :white_check_mark:  | :x:                 | :x:                 | [1B,2B,3B](activos/pdfs/Borrado_y_Modificacion.pdf)                         |
| 3.2.3     | Modificacion                                   | :white_check_mark:  | :x:                 | :white_check_mark:  | [1B,2B,3B](activos/pdfs/Borrado_y_Modificacion.pdf)                         |
| 3.3       | LISTAS SIMPLEMENTE ENLAZADAS                   | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | [1B,2B,3B](activos/pdfs/LISTAS_SIMPLEMENTE_ENLAZADAS.pdf)                   |
| 3.3.1     | Algoritmos sobre la lista                      | :x:                 | :x:                 | :x:                 | [1B,2B,3B](activos/pdfs/Algoritmos_sobre_la_lista.pdf)                      |
| 3.3.2     | Listas circulares                              | :x:                 | :x:                 | :x:                 | [1B,2B,3B](activos/pdfs/Listas_circulares.pdf)                              |
| 3.3.3     | Implementación en Lenguaje C++ con POO         | :x:                 | :white_check_mark:  | :white_check_mark:  | [1B,2B,3B](activos/pdfs/LSE-Implementacion_en_Lenguaje_CPP_con_POO.pdf)     |
| 3.4       | LISTAS DOBLEMENTE ENLAZADAS                    | :white_check_mark:  | :white_check_mark:  | :white_check_mark:  | [1B,2B,3B](activos/pdfs/LISTAS_DOBLEMENTE_ENLAZADAS.pdf)                    |
| 3.4.1     | Algoritmos sobre la lista doblemente enlazada  | :x:                 | :x:                 | :x:                 | [1B,2B,3B](activos/pdfs/Algoritmos_sobre_la_lista_doblemente_enlazada.pdf)  |
| 3.4.2     | Listas dobles circulares                       | :white_check_mark:  | :x:                 | :x:                 | [1B,2B,3B](activos/pdfs/CONCEPTOS_BASICOS.pdf)                              |
| 3.4.3     | Implementación en Lenguaje C++ con POO         | :x:                 | :white_check_mark:  | :white_check_mark:  | [1B,2B,3B](activos/pdfs/CONCEPTOS_BASICOS.pdf)                              |
|           | Horas Totales                                  | 8.0                 | 4.5                 | 8.0                 |                                                                             |

## ESTRATEGIA DIDÁCTICA

Resolución de ejercicios aplicando los diferentes tipos de listas enlazadas y discusión de las diferencias entre
ellas, coordinado por el profesor.
Implementará un programa para la solución de problemas de ingeniería empleando listas enlazadas

## PROCEDIMIENTO DE EVALUACIÓN

* Programas y ejercicios desarrollados en clase y extra clase.
* Examen del periodo.
